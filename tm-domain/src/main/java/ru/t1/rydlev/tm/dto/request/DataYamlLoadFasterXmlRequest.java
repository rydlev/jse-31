package ru.t1.rydlev.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataYamlLoadFasterXmlRequest extends AbstractUserRequest {

    public DataYamlLoadFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}
