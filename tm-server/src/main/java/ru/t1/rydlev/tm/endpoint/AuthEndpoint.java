package ru.t1.rydlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.rydlev.tm.api.service.IAuthService;
import ru.t1.rydlev.tm.api.service.IServiceLocator;
import ru.t1.rydlev.tm.dto.request.UserLoginRequest;
import ru.t1.rydlev.tm.dto.request.UserLogoutRequest;
import ru.t1.rydlev.tm.dto.request.UserViewProfileRequest;
import ru.t1.rydlev.tm.dto.response.UserLoginResponse;
import ru.t1.rydlev.tm.dto.response.UserLogoutResponse;
import ru.t1.rydlev.tm.dto.response.UserViewProfileResponse;
import ru.t1.rydlev.tm.model.Session;
import ru.t1.rydlev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.rydlev.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse loginUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logoutUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        final Session session = check(request);
        getServiceLocator().getAuthService().invalidate(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserViewProfileResponse viewUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = getServiceLocator().getUserService().findOneById(userId);
        return new UserViewProfileResponse(user);
    }

}
